package com.ifmo.igorjan94

import java.util.*
import kotlin.collections.HashMap

class Context {
    private var variables = HashMap<String, Set<Int>>()
    private var cells: MutableList<Cell> = ArrayList()

    fun defineFlip(name: String, p: Double) {
        defineMultinomial(
            name,
            mutableMapOf(
                0 to p,
                1 to 1 - p
            )
        )
    }

    fun defineMultinomial(name: String, ps: MutableMap<Int, Double>) {
        assertUniqueName(name)

        variables[name] = HashSet(ps.keys)

        if (cells.isEmpty())
            ps.map { (key, value) ->
                cells.add(Cell(value, name, key))
            }
        else {
            val newCells = ArrayList<Cell>()

            if (ps.isEmpty())
                throw IllegalArgumentException("Map shouldn't be empty")

            ps.map { (key, value) ->
                cells.map { cell ->
                    val newCell = Cell(cell.p * value, cell.variable)
                    newCell.addVariable(name, key)
                    newCells.add(newCell)
                }
            }

            cells = newCells
        }
    }

    fun infer(name: String): Map<Int, Double> {
        return variables[name]!!.associateBy({ it }, { value ->
            cells.filter { cell -> cell.variable[name] == value }.map { it.p }.sum()
        })
    }

    fun observe(function: (MutableMap<String, Int>) -> Boolean) {
        var sum = 0.0
        cells.map { cell ->
            if (!function(cell.variable))
                cell.p = 0.0
            else
                sum += cell.p
        }

        cells.map { cell -> cell.p /= sum }
    }

    private fun assertUniqueName(name: String) {
        if (variables.containsKey(name))
            throw IllegalArgumentException("Name $name already exists")
    }
}