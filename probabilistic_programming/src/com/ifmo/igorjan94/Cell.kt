package com.ifmo.igorjan94


import java.util.HashMap

class Cell {
    var p: Double
    var variable: MutableMap<String, Int>

    constructor(p: Double, name: String, value: Int) {
        this.p = p
        this.variable = HashMap(mapOf(name to value))
    }

    constructor(p: Double, variable: Map<String, Int>) {
        this.p = p
        this.variable = HashMap(variable)
    }

    fun addVariable(name: String, value: Int) {
        if (variable.containsKey(name))
            throw IllegalArgumentException("Name $name already exists")
        variable[name] = value
    }
}