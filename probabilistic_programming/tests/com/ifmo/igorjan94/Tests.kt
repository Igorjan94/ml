package com.ifmo.igorjan94

import org.junit.Test

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue

class Tests {

    @Test
    fun flip() {
        val context = Context()
        context.defineFlip("d1", 0.3)

        val actual = context.infer("d1")
        val expected = mapOf(
            0 to 0.3,
            1 to 0.7
        )

        assertEqualsMap("Invalid 'd1' probability", expected, actual)
    }

    @Test
    fun multinomial() {
        val context = Context()
        val mapD2 = mutableMapOf(
            2 to 0.1,
            3 to 0.4,
            200 to 0.5
        )
        context.defineMultinomial("d2", mapD2)

        val actual = context.infer("d2")
        val expected = mapOf(
            2 to 0.1,
            3 to 0.4,
            200 to 0.5
        )

        assertEqualsMap("Invalid 'd2' probability", expected, actual)
    }

    @Test
    fun multipleDefinitionsInOneContext() {
        val context = Context()
        context.defineFlip("d1", 0.3)

        val mapD2 = mutableMapOf(
            2 to 0.1,
            3 to 0.4,
            200 to 0.5
        )
        context.defineMultinomial("d2", mapD2)

        val mapD3 = mutableMapOf(
            2 to 0.2,
            3 to 0.3,
            200 to 0.5
        )
        context.defineMultinomial("d3", mapD3)

        val expectedD1 = mapOf(
            0 to 0.3,
            1 to 0.7
        )

        val expectedD2 = mapOf(
            2 to 0.1,
            3 to 0.4,
            200 to 0.5
        )

        val expectedD3 = mapOf(
            2 to 0.2,
            3 to 0.3,
            200 to 0.5
        )

        val actualD1 = context.infer("d1")
        val actualD2 = context.infer("d2")
        val actualD3 = context.infer("d3")

        assertEqualsMap("Invalid 'd1' probability", expectedD1, actualD1)
        assertEqualsMap("Invalid 'd2' probability", expectedD2, actualD2)
        assertEqualsMap("Invalid 'd3' probability", expectedD3, actualD3)
    }


    @Test
    fun honestCoinsWithObserve() {
        val context = Context()
        context.defineFlip("d1", 0.5)
        context.defineFlip("d2", 0.5)

        context.observe { map -> map["d1"] == 1 || map["d2"] == 1 }

        val expectedD1 = mapOf(
            0 to 0.33333,
            1 to 0.66667
        )

        val expectedD2 = mapOf(
            0 to 0.33333,
            1 to 0.66667
        )

        val actualD1 = context.infer("d1")
        val actualD2 = context.infer("d2")

        assertEqualsMap("Invalid 'd1' probability", expectedD1, actualD1)
        assertEqualsMap("Invalid 'd2' probability", expectedD2, actualD2)

        context.observe { map -> map["d1"] == 1 }

        assertEqualsMap("Invalid 'd1' probability", mapOf(0 to 0.0, 1 to 1.0), context.infer("d1"))
    }

    @Test
    fun observe() {
        val context = Context()
        context.defineFlip("d1", 0.3)
        context.defineFlip("d2", 0.5)

        context.observe { map -> map["d1"] == 1 || map["d2"] == 1 }

        val expectedD1 = mapOf(
            0 to 0.17647,
            1 to 0.82352
        )

        val expectedD2 = mapOf(
            0 to 0.41176,
            1 to 0.58823
        )

        val actualD1 = context.infer("d1")
        val actualD2 = context.infer("d2")

        assertEqualsMap("Invalid 'd1' probability", expectedD1, actualD1)
        assertEqualsMap("Invalid 'd2' probability", expectedD2, actualD2)
    }


    @Test
    fun observeFlipMultinomial() {
        val context = Context()
        context.defineFlip("d1", 0.3)

        val mapD2 = mutableMapOf(
            2 to 0.1,
            3 to 0.3,
            4 to 0.6
        )
        context.defineMultinomial("d2", mapD2)

        context.observe { map -> map["d1"]!! + map["d2"]!! >= 4 }

        val expectedD1 = mapOf(
            0 to 0.22222,
            1 to 0.77777
        )

        val expectedD2 = mapOf(
            2 to 0.0,
            3 to 0.25926,
            4 to 0.74074
        )

        val actualD1 = context.infer("d1")
        val actualD2 = context.infer("d2")

        assertEqualsMap("Invalid 'd1' probability", expectedD1, actualD1)
        assertEqualsMap("Invalid 'd2' probability", expectedD2, actualD2)
    }

    private fun assertEqualsMap(msg: String, expected: Map<Int, Double>, actual: Map<Int, Double>, precision: Double = 0.0001) {
        assertEquals("$msg: invalid size", expected.size, actual.size)

        expected.entries.forEach { (key, value) ->
            assertTrue(String.format("%s: result doesn't contain %d", msg, key), actual.containsKey(key))
            assertTrue(msg, Math.abs(value - actual[key]!!) < precision)
        }
    }

}