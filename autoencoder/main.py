import tensorflow as tf
import tensorflow.examples.tutorials.mnist.input_data as input_data

import numpy as np
import matplotlib.pyplot as plt

def plot_digits(*args):
    args = [x.squeeze() for x in args]
    n = min([x.shape[0] for x in args])
    plt.figure(figsize = (2 * n, 2 * len(args)))
    for j in range(n):
        for i in range(len(args)):
            ax = plt.subplot(len(args), n, i * n + j + 1)
            plt.imshow(args[i][j])
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
    plt.show()

class Autoencoder:
    def leak(self, x):
        p = 0.175
        f1 = 0.5 * (1 + p)
        f2 = 0.5 * (1 - p)
        return f1 * x + f2 * abs(x)

    def getTensor(self, x):
        if len(x.get_shape()) == 2:
            dim = x.get_shape().as_list()[1]
            sqrt_dim = int(np.sqrt(dim))
            if sqrt_dim ** 2 != dim:
                raise ValueError('Unsupported input dimensions')
            return tf.reshape(x, [-1, sqrt_dim, sqrt_dim, self.n_filters[0]])
        elif len(x.get_shape()) == 4:
            return x
        else:
            raise ValueError('Unsupported input dimensions')

    def __init__(self, **args):
        self.input_shape = [None, 784]
        self.n_filters = [1, 10, 10, 10]
        self.filter_sizes = [3, 3, 3, 3]

        if 'input_shape' in args:
            self.input_shape = args['input_shape']
        if 'n_filters' in args:
            self.n_filters = args['n_filters']
        if 'filter_sizes' in args:
            self.filter_sizes = args['filter_sizes']

        x = tf.placeholder(tf.float32, self.input_shape, name = 'x')
        x_tensor = self.getTensor(x)

        current_input = x_tensor
        encoder = []
        shapes = []
        for layer_i, n_output in enumerate(self.n_filters[1:]):
            n_input = current_input.get_shape().as_list()[3]
            shapes.append(current_input.get_shape().as_list())
            w = tf.Variable(
                tf.random_uniform([
                    self.filter_sizes[layer_i],
                    self.filter_sizes[layer_i],
                    n_input,
                    n_output
                ],
                -1 / np.sqrt(n_input),
                1 / np.sqrt(n_input))
            )
            b = tf.Variable(tf.zeros([n_output]))
            encoder.append(w)
            output = self.leak(tf.add(tf.nn.conv2d(current_input, w, strides = [1, 2, 2, 1], padding = 'SAME'), b))
            current_input = output

        encoder.reverse()
        shapes.reverse()

        for (w, shape) in zip(encoder, shapes):
            b = tf.Variable(tf.zeros([w.get_shape().as_list()[2]]))
            output = self.leak(
                tf.add(
                    tf.nn.conv2d_transpose(
                        current_input,
                        w,
                        tf.stack([
                            tf.shape(x)[0],
                            shape[1],
                            shape[2],
                            shape[3]
                        ]),
                        strides = [1, 2, 2, 1],
                        padding = 'SAME'
                    ),
                    b
                )
            )
            current_input = output

        self.x = x
        self.y = current_input
        self.cost = tf.reduce_sum(tf.square(self.y - x_tensor))
        self.session = tf.Session()

    def fit(self, x_train, **args):
        self.batch_size = 256
        self.n_epochs = 10
        self.learning_rate = 0.01

        if 'batch_size' in args:
            self.batch_size = args['batch_size']
        if 'n_epochs' in args:
            self.n_epochs = args['n_epochs']
        if 'learning_rate' in args:
            self.learning_rate = args['learning_rate']

        optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(self.cost)

        self.session.run(tf.global_variables_initializer())

        for epoch_i in range(self.n_epochs):
            for _ in range(x_train.num_examples // self.batch_size):
                train, _ = x_train.next_batch(self.batch_size)
                self.session.run(optimizer, feed_dict = {self.x: train})
            print(epoch_i, self.session.run(self.cost, feed_dict = {self.x: train}))

    def predict(self, x_test):
        return self.session.run(self.y, feed_dict = {self.x: x_test})

def res(x):
    return np.reshape(x, (len(x), 28, 28, 1))

def test():
    n_examples = 10
    mnist = input_data.read_data_sets('MNIST_data')

    train = mnist.train
    test, _ = mnist.test.next_batch(n_examples)
    ae = Autoencoder()
    ae.fit(train, batch_size = 256, n_epochs = 15)
    digits = ae.predict(test)

    plot_digits(res(test), digits)


if __name__ == '__main__':
    test()
